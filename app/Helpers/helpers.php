<?php
use App\Interfaces\StatusCodes;

/**
 * Return a new JSON response with mixed(object|JsonSerializable) data
 *
 * @param int $status
 * @param mixed $payload
 * @param string|null $message
 * @return Illuminate\Http\JsonResponse
 */
function apiResponse(int $status, $payload, $message = null): \Illuminate\Http\JsonResponse {
        if (in_array($status, [StatusCodes::OK, StatusCodes::CREATED, StatusCodes::UPDATED, StatusCodes::DELETED])){
            $response = [
                'status' => $status,
                'data' => $payload,
                'message' => $message
            ];
        } else {
            $response = [
                'status' => $status,
                'errors' => $payload,
                'message' => $message
            ];
        }
        return response()->json($response, $status);
}
