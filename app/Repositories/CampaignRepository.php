<?php
namespace App\Repositories;
use App\Interfaces\StatusCodes;
use App\Models\Campaign;
use Illuminate\Support\Facades\DB;

class CampaignRepository
{
    private $model;

    public function __construct(Campaign $campaign) {
        $this->model = $campaign;
    }

    public function addNewCampaign($payload){
        //start transaction
        DB::beginTransaction();
        $addedCampaignStatus = $this->model->firstOrCreate($payload);

        if (!$addedCampaignStatus) {
            DB::rollBack();
            return apiResponse(StatusCodes::SERVER_ERROR, [], 'Something went wrong and campaign wasn\'t added');
        }

        DB::commit();
        return apiResponse(StatusCodes::CREATED, [], 'Campaign successfully created');
    }


    public function updateExistingCampaign($payload){
        //start transaction
        DB::beginTransaction();
        $addedCampaignStatus = $this->model->where('id', $payload['id'])->update($payload);

        if (!$addedCampaignStatus) {
            DB::rollBack();
            return apiResponse(StatusCodes::SERVER_ERROR, [], 'Something went wrong and campaign wasn\'t updated');
        }

        DB::commit();
        return apiResponse(StatusCodes::UPDATED, [], 'Campaign successfully updated');
    }

    public function getCampaignsList(){
        return $this->model->paginate(20);
    }

    public function findById(int $id){
        $campaign = $this->model->find($id);
        if (is_null($campaign)) return apiResponse(StatusCodes::NOT_FOUND, [], 'Campaign not found');
        return $campaign;
    }

    public function deleteCampaign(int $id){
        $campaignDeleteStatus = $this->model->where('id', $id)->delete();
        if (!$campaignDeleteStatus) return apiResponse(StatusCodes::NOT_FOUND, [], 'Campaign something went wrong and campaign cannot be deleted');
        return apiResponse(StatusCodes::DELETED, [], 'Campaign successfully deleted');
    }

}
