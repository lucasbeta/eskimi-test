<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCampaignRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string'],
            'total_budget' => 'required|numeric',
            'daily_budget' => 'required|numeric',
            'banners' => 'required',
        ];
    }
}
