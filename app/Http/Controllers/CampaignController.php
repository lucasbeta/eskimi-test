<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCampaignRequest;
use App\Http\Requests\UpdateCampaignRequest;
use App\Interfaces\StatusCodes;
use App\Repositories\CampaignRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class CampaignController extends Controller
{
    protected $campaignRepository;

    public function __construct(CampaignRepository $campaignRepository
    ){
        $this->campaignRepository = $campaignRepository;
    }

    public function createCampaign(CreateCampaignRequest $request): JsonResponse
    {
        $payload = $request->all();
        $payload['banners'] = json_encode($this->uploadBanners($request->file('banners')));

        return $this->campaignRepository->addNewCampaign($payload);
    }


    public function updateCampaign(UpdateCampaignRequest $request): JsonResponse
    {
        $payload = $request->all();
        $newFiles = $this->uploadBanners($request->file('banners'));
        $payload['banners'] = json_encode(array_merge(is_array($newFiles) ? $newFiles : [], is_array($request->existingFiles) ? $request->existingFiles : []));
        unset($payload['existingFiles']);

        return $this->campaignRepository->updateExistingCampaign($payload);
    }


    public function viewCampaign(Request $request)
    {
        return $this->campaignRepository->findById($request->id);
    }


    public function deleteCampaign(Request $request)
    {
        return $this->campaignRepository->deleteCampaign($request->id);
    }


    public function uploadBanners($files) {
        if (!$files){
            return apiResponse(StatusCodes::BAD_REQUEST, [], 'Please select images to upload');
        }
        $images = [];
        foreach ($files as $file) {
            $time = date_format(date_create(), 'YmdHis');
            $imageName = $time . '-' . $file->getClientOriginalName();
            $file->move(public_path() . '/uploads/', $imageName);
            $images[] = $imageName;
        }

        return $images;
    }


    public function listCampaigns(): LengthAwarePaginator
    {
        return $this->campaignRepository->getCampaignsList();
    }
}
