import CampaignList from "../components/CampaignList";
import CampaignForm from "../components/CampaignForm";

const appRoutes = [
    {
        path: '/',
        name: 'Campaign.Management',
        component: CampaignList,
    },
    {
        path: '/campaigns/create',
        name: 'Campaign.Management.Create',
        component: CampaignForm,
    },
    {
        path: '/campaigns/edit/:id',
        name: 'Campaign.Management.Edit',
        component: CampaignForm,
    },
];

export default appRoutes;
