/**
 * Function rely on Vue, VueStore and Vuex being in global scope.. in most cases window object
 *
 * @param option
 * @returns {undefined|Vue|CombinedVueInstance<Vue, object, object, object, Record<never, any>>}
 */
const mountCreator = (option = {}) => {
    const optionProps = Object.keys(option);
    const vueOption = {};

    // make sure mountName is never empty on use of this factory function
    if (!optionProps.includes('mountName'))
        throw new Error(`'mountName' constructor prop is required!`);
    const mountName = option['mountName'];
    vueOption.el = `#${mountName}`;

    if (document.getElementById(mountName)) { // save from system memory if element is not found
        // set the route needed for the function.
        if (optionProps.includes('routes') && Array.isArray(option['routes'])) {
            // VueRouter must be imported in global scope
            vueOption.router = new VueRouter({
                routes: option['routes'], // short for `routes: routes`
            });
        }

        // Vue must be imported in global scope
        return new Vue(vueOption);
    }

    return undefined;
};

export {
    mountCreator
}
