import AppLayout from "../components/layout/AppLayout";

const Components = {
     AppLayout
};

Object
    .keys(Components)
    .forEach(name => {
        Vue.component(name, Components[name]);
    });

export default Components;
