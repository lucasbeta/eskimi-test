import axios from 'axios';

function interceptorCallbacks (config, configCallbacks) {
    if (!configCallbacks ||  !Array.isArray(configCallbacks)) {
        throw new Error("callbacks should be an array of functions");
    }
    configCallbacks.forEach((cb) => {
        if(typeof cb !== "function"){
            throw new Error("callback should be a function");
        }
        cb(config);
    });
}
/**
 *
 * @param defaultConfig
 * @returns {AxiosStatic}
 * @constructor
 */
function AxiosService(defaultConfig = {}) {
    // Create a new axios instance
    const Axios = axios.create({
        baseURL: defaultConfig.baseUrl ? defaultConfig.baseUrl : '/',
    });
    // Things to configure with the options params : ENDS HERE
    const { interceptors } = defaultConfig;

    Axios.defaults.headers.common = {
        ...Axios.defaults.headers.common,
        'X-Requested-With': 'XMLHttpRequest',
        ...defaultConfig.headers.common,
    };


    // HttpService HTTP request Interceptor
    Axios.interceptors.request.use(
        (config) => {

            if (interceptors) {
                const [configCallbacks] = interceptors.request;
                // Call config request callback functions
                interceptorCallbacks(config, configCallbacks);
            }


            return config;
        },
        (e) => {
            if (interceptors) {
                const [_, errorCallbacks] = interceptors.request;
                // Call config request error callback functions
                interceptorCallbacks(e, errorCallbacks);
            }
            throw e;
        },
    );

    // HttpService HTTP response Interceptor
    Axios.interceptors.response.use(
        (r) => {
            if (interceptors) {
                const [configCallbacks] = interceptors.response;
                // Call config response error callback functions
                interceptorCallbacks(r, configCallbacks);
            }
            return r;
        },

        (e) => {
            if (interceptors) {
                const [_, errorCallbacks] = interceptors.response;
                // Call config response error callback functions
                interceptorCallbacks(e, errorCallbacks);
            }

           // Always throw exception
            throw e;
        },
    );

    return Axios;
}

export default AxiosService;
