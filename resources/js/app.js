/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueRouter from 'vue-router';
import moment from "moment/moment";
import {mountCreator} from "./module/Init";
import AxiosConfig from "./config/AxiosConfig";
import AxiosService from "./services/AxiosService";
import VCalendar from 'v-calendar';
import VueLazyLoad from 'vue-lazyload'
import VueIziToast from 'vue-izitoast';
import 'izitoast/dist/css/iziToast.min.css';

require('vue-image-lightbox/dist/vue-image-lightbox.min.css')


window.Vue = require('vue').default;
window.VueRouter = VueRouter;

Vue.use(VueRouter);
Vue.use(VCalendar);
Vue.use(VueLazyLoad);
Vue.use(VueIziToast);
Vue.prototype.$httpRequest = AxiosService(
    AxiosConfig
);
Vue.prototype.$moment = moment

// register module global component
require('./module/Components');

// mount the vueJs App here...
mountCreator({
    mountName: 'testAppMountingPoint',
    routes: require(`./module/Routes`).default,
});
