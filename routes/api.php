<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'prefix' => '/campaigns',
    'namespace' => 'App\Http\Controllers'
], function (){
    Route::get('/', 'CampaignController@listCampaigns');
    Route::post('/create', 'CampaignController@createCampaign');
    Route::post('/update/{id}', 'CampaignController@updateCampaign');
    Route::get('/view/{id}', 'CampaignController@viewCampaign');
    Route::delete('/delete/{id}', 'CampaignController@deleteCampaign');
});
