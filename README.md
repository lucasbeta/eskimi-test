## About app

This ia a basic app that allows creating, updating and deleting campaigns with creative preview.

## Prerequisites
- Git
- Docker [ >= 17.12 ]
- Php [ >= 7.3 ]
- Node [ >= 14.16.1 ]

## Installation
- ``git clone https://git@bitbucket.org/lucasbeta/eskimi-test.git`` or ssh ``git clone git@bitbucket.org:lucasbeta/eskimi-test.git``
- ``cd eskimi-test``
- ``git submodule init && git submodule update``
- ``cp .env.example .env``
- ``composer install``
- ``php artisan key:generate``
- ``npm install``
- ``npm run dev``
- ``cd laradock`` from the root dir
- ``cp .env.example .env``
- ``docker-compose up -d nginx mysql`` 
- ``docker-compose exec mysql bash`` 
- ``mysql --user=root --password=root`` within mysql bash
- ``CREATE DATABASE eskimi`` within mysql bash, change eskimi to whatever database you set in laravel's .env
  Set ``DB_USERNAME=root``
  ``DB_PASSWORD=root`` and ``DB_HOST=mysql`` in laravel's env... user name and password can be modified in laradock's env then changed in laravel's env after which image should be rebuilt.

- Exit mysql bash then:ßßß
- ``docker-compose exec workspace bash``
- ``php artisan migrate``
- ``docker-compose down``
- ``docker-compose up -d nginx mysql``


## Tests
There's test for both creating and updating campaigns
``./vendor/bin/phpunit ./tests/Unit/testCampaignCrud.php``

Or just run the tests within IDE in tests/Unit directory.

Visit ``localhost``
